This document describes how the workflow builder is designed. It describes how the dynamic form can be created by dragging form components.


Requirements:-
--------------
The application is designed and implemented on the following business requirements:
	* Easy to use
	* Application should work even after loading external json file
	* Should be robust to save workflow in file storge and browser's local storage
	

Technology:-
-------------
Application is implemented based on following technologies:
	* React


Functional Description:-
------------------------
Once the application is loaded it will show form fields (Which are draggable) on left and two buttons called Load Json File and Start new on the right side.
Click on start new will show you a modal asking to name a step.
Once the stepper is named, then drop area is loaded and ready to accumulate form fields. Along with drop area a save button is also be shown at the bottom of the drop area to save workflow to a file system as json.
On every field drag, a popup shown where we need to fill the label, placeholder and styles. 
All these fields are not mandatory at all. Based on our needs we can fill these fields. Styles will be applied to particular field.
Dragging a stepper field will always create new workflow/form.
Whatever the fields are dragged all those data are stored to localStorage of the browser along field properties like label, placeholder and styles etc.
We can even store the workflow in our filestorage by clicking on save button which will downoad the workflow to your file system as file.
If we reload the application then application will fetch workflow data from localstorage and renders it.
If somehow localstorage vanished then we can load from external json file.


Technical Specification:-
-------------------------
formBuilder.js	:-	This is the component which is responsible to create main layou of the application.
dragArea.js		:-	This component is used to create list of form components like Header Text, Text Input, Date and Phone etc.
dropArea.js		:-	This component will accumulate all the dragged fields.

Above mentioned components are the main components and rest all components are normal components about showing popup modal.


NPM Libraries used:-
---------------------
react-dnd						:-	For Drag and Drop of fields.
react-dnd-html5-backend			:-	For Drag and Drop of fields.
react-stepzilla					:-	For creating steppers.
semantic-ui-react				:-	For use of existing react components.
semantic-ui-css					:-	For adding css to semantic components.
uuid							:-	For creating a unique id.
react-phone-input-2				:-	For creating phone input field.
react-semantic-ui-datepickers	:-	For creating date picker field.
file-saver						:-	For writing file to file storage.
lodash							:-	Utility library.


		 

