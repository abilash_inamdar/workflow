import React from "react";
import { DndProvider } from "react-dnd";
import Backend from "react-dnd-html5-backend";
import FormBuilder from "./formBuilder";

function App() {
  return (
    <DndProvider backend={Backend}>
      <FormBuilder />
    </DndProvider>
  );
}

export default App;
