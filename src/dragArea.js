import React from "react";
import FormField from "./formField";
import FieldTypes from "./fieldTypes";

const FIELDS = [
  {
    icon: "arrows alternate horizontal",
    label: "Stepper",
    field: FieldTypes.STEPPER
  },
  {
    icon: "header",
    label: "Header Text",
    field: FieldTypes.HEADER
  },
  {
    icon: "font",
    label: "Text Input",
    field: FieldTypes.INPUT
  },
  {
    icon: "text height",
    label: "Multi-line Input",
    field: FieldTypes.MULTI_TEXT
  },
  {
    icon: "calendar alternate",
    label: "Date",
    field: FieldTypes.DATE
  },
  {
    icon: "phone",
    label: "Phone",
    field: FieldTypes.PHONE
  },
  {
    icon: "table",
    label: "Table",
    field: FieldTypes.TABLE
  }
];

function DragArea() {
  return (
    <div style={{ width: "20%" }}>
      {FIELDS.map(field => (
        <FormField icon={field.icon} label={field.label} field={field.field} />
      ))}
    </div>
  );
}

export default DragArea;
