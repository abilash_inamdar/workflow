import React, { useState } from "react";
import _ from "lodash";
import { useDrop } from "react-dnd";
import uuidv4 from "uuid/v4";
import SemanticDatepicker from "react-semantic-ui-datepickers";
import PhoneInput from "react-phone-input-2";
import {
  Header,
  Input,
  TextArea,
  Form,
  Table,
  Label,
  Button,
  Icon,
} from "semantic-ui-react";
import FieldTypes from "./fieldTypes";
import FieldPropModal from "./fieldPropModal";
import FileSaver from "file-saver";

const getField = (field) => {
  const {
    type,
    fieldProps: { placeholder, label, header, rows, cols, styles },
  } = field;

  const style = _.reduce(
    styles,
    (styleObject, style) => {
      styleObject[style.key] = style.value;
      return styleObject;
    },
    {}
  );
  switch (field.field) {
    case FieldTypes.INPUT:
      return (
        <Form>
          <Form.Field>
            <label>{label}</label>
            <input placeholder={placeholder} style={style} />
          </Form.Field>
        </Form>
      );
    case FieldTypes.MULTI_TEXT:
      return (
        <Form>
          <label>{label}</label>
          <TextArea placeholder={placeholder} style={style} />
        </Form>
      );
    case FieldTypes.DATE:
      return (
        <Form>
          <label>{label}</label>
          <SemanticDatepicker style={style} />
        </Form>
      );
    case FieldTypes.PHONE:
      return (
        <Form.Field>
          <label>{label}</label>
          <PhoneInput country="us" style={style} />
        </Form.Field>
      );
    case FieldTypes.HEADER:
      return (
        <Header as="h2" style={style}>
          {header}
        </Header>
      );
    case FieldTypes.TABLE:
      return (
        <Table celled style={style}>
          <Table.Header>
            <Table.Row>
              {_.map(_.range(+cols), (col, idx) => (
                <Table.HeaderCell>Cell</Table.HeaderCell>
              ))}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {_.map(_.range(+rows - 1), (row, idx) => (
              <Table.Row>
                {_.map(_.range(+cols), (col, idx) => (
                  <Table.Cell>Cell</Table.Cell>
                ))}
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      );
  }
};

function DropArea(props) {
  let workflowJson = localStorage.getItem("workflow");
  workflowJson = JSON.parse(workflowJson);

  let fieldsFromJson = workflowJson ? workflowJson[props.stepId] || [] : [];
  const [fields, addField] = useState(fieldsFromJson);
  const [openFieldPropModal, setOpenFieldPropModal] = useState(false);
  const [draggedField, setDraggedField] = useState(null);
  const [editing, setEditing] = useState(false);

  const onFieldDrop = (arg) => {
    // if stepper field dragged then create a new form
    if (arg.field === FieldTypes.STEPPER) {
      props.createNewForm();
      return;
    }
    // show alert when there is no stepper field
    if (!fields.length && !props.stepId) {
      alert("Please add stepper field to begin..");
      return;
    }
    setDraggedField(arg);
    setOpenFieldPropModal(true);
  };

  const renderField = (fieldProps) => {
    if (!fieldProps) {
      setOpenFieldPropModal(false);
      return;
    }
    const id = editing ? draggedField.id : uuidv4();
    if (!editing) {
      draggedField.id = draggedField.groupId = id;
    }
    const prevField = editing
      ? _.get(fields, `[${_.findIndex(fields, ["id", id]) - 1}]`)
      : _.get(fields, `[${fields.length - 1}]`);

    draggedField.groupId = fieldProps.checked
      ? _.get(prevField, "groupId", id)
      : id;
    if (editing) {
      draggedField.fieldProps = fieldProps;
    }
    const allFields = [...fields];
    if (!editing) {
      allFields.push({ ...draggedField, fieldProps });
    }
    setJson(allFields);
    addField(allFields);
    setEditing(false);
    setOpenFieldPropModal(false);
  };

  const setJson = (fields) => {
    let workflowJson = localStorage.getItem("workflow");
    workflowJson = JSON.parse(workflowJson);
    workflowJson[props.stepId] = fields;
    localStorage.setItem("workflow", JSON.stringify(workflowJson));
  };

  const editField = (field) => () => {
    setDraggedField(field);
    setEditing(true);
    setOpenFieldPropModal(true);
  };

  const deleteField = (field) => () => {
    _.remove(fields, field);
    setJson(fields);
    addField([...fields]);
  };

  const saveFile = () => {
    const blob = new Blob([localStorage.getItem("workflow")], {
      type: "text/plain;charset=utf-8",
    });

    FileSaver.saveAs(blob, `${props.stepId}.json`);
  };

  const deleteForm = () => {
    let workflowJson = localStorage.getItem("workflow");
    workflowJson = JSON.parse(workflowJson);
    workflowJson = _.omit(workflowJson, props.stepId);
    localStorage.setItem("workflow", JSON.stringify(workflowJson));
    props.deleteForm && props.deleteForm();
  };

  const [, drop] = useDrop({
    accept: "FORMFIELD",
    drop: onFieldDrop,
    collect: (monitor) => {
      return {
        isOver: !!monitor.isOver(),
        canDrop: !!monitor.canDrop(),
      };
    },
  });
  const fieldsByGropId = _.groupBy(fields, "groupId");
  return (
    <>
      <div
        ref={drop}
        style={{
          height: "100%",
          border: "1px solid #ddd",
          background: "#fafafa",
          padding: "10px",
        }}
      >
        {_.map(fieldsByGropId, (items, groupId) => (
          <div
            style={{
              display: "flex",
              alignItems: "center",
              marginBottom: "15px",
              padding: "0px 10px",
            }}
            key={groupId}
          >
            {_.map(items, (field) => (
              <div
                key={field.id}
                style={{
                  width: "100%",
                  padding: "0px 10px",
                }}
              >
                {getField(field)}
                <Icon
                  name="edit"
                  onClick={editField(field)}
                  style={{ cursor: "pointer" }}
                />
                <Icon
                  name="delete"
                  onClick={deleteField(field)}
                  style={{ cursor: "pointer", marginLeft: "5%" }}
                />
              </div>
            ))}
          </div>
        ))}
        {openFieldPropModal && (
          <FieldPropModal field={draggedField} onClose={renderField} />
        )}
      </div>
      <Button primary onClick={deleteForm}>
        Delete form
      </Button>
      <Button primary onClick={props.deleteAllForms}>
        Delete All Forms
      </Button>
      <Button primary onClick={saveFile} style={{ float: "right" }}>
        Save to file
      </Button>
      <Button primary onClick={props.renameForm} style={{ float: "right" }}>
        Rename Form
      </Button>
    </>
  );
}

export default DropArea;
