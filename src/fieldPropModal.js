import React, { useState, useEffect } from "react";
import { Modal, Button, Header, Form, Icon, Checkbox } from "semantic-ui-react";
import uuid from "uuid/v4";
import _ from "lodash";
import FieldTypes from "./fieldTypes";

function FieldPropModal({ field: { field, fieldProps }, onClose }) {
  useEffect(() => {
    if (fieldProps) {
      setLabel(fieldProps.label);
      setChecked(fieldProps.checked);
      setPlaceholder(fieldProps.placeholder);
      setHeader(fieldProps.header);
      setRows(fieldProps.rows);
      setCols(fieldProps.cols);
      setStyles(
        _.map(fieldProps.styles, (style) => ({
          id: uuid(),
          value: { ...style },
        }))
      );
    }
  }, []);
  const [label, setLabel] = useState("");
  const [checked, setChecked] = useState(false);
  const [placeholder, setPlaceholder] = useState("");
  const [header, setHeader] = useState("");
  const [rows, setRows] = useState(0);
  const [cols, setCols] = useState(0);
  const [styles, setStyles] = useState([
    { id: uuid(), value: { key: "", value: "" } },
  ]);
  const [errorType, setErrorType] = useState("");

  const setStyleProps = (style, key, value) => {
    style.value[key] = value;
    setStyles([...styles]);
  };

  const addNewStyleRow = () => {
    setStyles([...styles, { id: uuid(), value: { key: "", value: "" } }]);
  };

  const fieldStyle = {
    marginBottom: "10px",
  };

  const closeModal = () => {
    if (field === FieldTypes.HEADER) {
      if (!header.trim().length) {
        setErrorType(FieldTypes.HEADER);
        return;
      }
    } else {
      if (!label.trim().length) {
        setErrorType("label");
        return;
      }
      if (field === FieldTypes.TABLE) {
        if (rows <= 0) {
          setErrorType("rows");
          return;
        }
        if (cols <= 0) {
          setErrorType("cols");
          return;
        }
      }
    }

    onClose({
      checked,
      label,
      placeholder,
      header,
      rows,
      cols,
      styles: _.map(styles, "value"),
    });
  };

  return (
    <Modal open closeIcon onClose={() => onClose()}>
      <Modal.Header>{field}</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Form>
            {/* show label except header text */}
            {field !== FieldTypes.HEADER && (
              <Form.Input
                error={errorType == "label"}
                fluid
                label="Enter the label to appear on the field"
                placeholder="Label"
                value={label}
                style={fieldStyle}
                onChange={(e) => {
                  setLabel(e.target.value);
                  setErrorType("");
                }}
              />
            )}
            {/* show placeholder only for text fields */}
            {(field === FieldTypes.INPUT ||
              field === FieldTypes.MULTI_TEXT) && (
              <Form.Input
                fluid
                label="Enter the placeholder to appear in the field"
                placeholder="Placeholder"
                value={placeholder}
                style={fieldStyle}
                onChange={(e) => setPlaceholder(e.target.value)}
              />
            )}
            {/* show this input to enter header text */}
            {field === FieldTypes.HEADER && (
              <Form.Input
                error={errorType == FieldTypes.HEADER}
                fluid
                label="Enter header text..."
                placeholder="header"
                value={header}
                style={fieldStyle}
                onChange={(e) => {
                  setHeader(e.target.value);
                  setErrorType("");
                }}
              />
            )}
            {field === FieldTypes.TABLE && (
              <Form.Group widths="equal">
                <Form.Field
                  error={
                    errorType == "rows" &&
                    "Enter the no. of rows (should start from 1) "
                  }
                  label="Rows"
                  control="input"
                  type="number"
                  max={5}
                  value={rows}
                  onChange={(e) => {
                    setRows(e.target.value);
                    setErrorType("");
                  }}
                />
                <Form.Field
                  error={
                    errorType == "cols" &&
                    "Enter the no. of columns (should start from 1) "
                  }
                  label="Cols"
                  control="input"
                  type="number"
                  max={5}
                  value={cols}
                  onChange={(e) => {
                    setCols(e.target.value);
                    setErrorType("");
                  }}
                />
              </Form.Group>
            )}
            <Checkbox
              label="Position horizontally to previous field"
              checked={checked}
              onChange={(e) => setChecked(!checked)}
            />
            <div>
              <Header as="h3" style={{ marginTop: "50px" }}>
                Styles
              </Header>
              <Form>
                {_.map(styles, (style) => {
                  return (
                    <Form.Group widths="equal">
                      <Form.Input
                        fluid
                        label="Key"
                        placeholder="Key"
                        value={style.value.key}
                        onChange={(e) =>
                          setStyleProps(style, "key", e.target.value)
                        }
                      />
                      <Form.Input
                        fluid
                        label="Value"
                        placeholder="Value"
                        value={style.value.value}
                        onChange={(e) =>
                          setStyleProps(style, "value", e.target.value)
                        }
                      />
                      <Icon name="add circle" onClick={addNewStyleRow} />
                    </Form.Group>
                  );
                })}
              </Form>
            </div>
            <div style={{ textAlign: "center", marginTop: "10px" }}>
              <Button primary onClick={closeModal}>
                Done
              </Button>
            </div>
          </Form>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
}

export default FieldPropModal;
