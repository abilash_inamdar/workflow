const FieldTypes = {
  STEPPER: 'stepper',
  HEADER: "header",
  INPUT: "text",
  MULTI_TEXT: "multitext",
  DATE: "date",
  PHONE: "phone",
  TABLE: 'table'
};

export default FieldTypes;
