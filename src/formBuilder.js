import React, { useState, useRef } from "react";
import _ from "lodash";
import StepZilla from "react-stepzilla";
import DragArea from "./dragArea";
import DropArea from "./dropArea";
import StepperModal from "./stepperModal";
import uuidv4 from "uuid/v4";
import { Button } from "semantic-ui-react";

function FormBuilder() {
  // get steps from localstorage and update the state
  let workflowJson = localStorage.getItem("workflow");
  workflowJson = JSON.parse(workflowJson);
  const stepsFromJson = workflowJson ? workflowJson.steps : [];

  const [steps, addStep] = useState(stepsFromJson);
  const [selectedStep, setSelectedStep] = useState(null);
  const [stepperModalOpen, openStepperModal] = useState(false);
  const fileInputRef = useRef(null);

  const createNewForm = () => {
    openStepperModal(true);
  };

  const deleteForm = (step) => () => {
    // update local storage by deleting step
    _.remove(steps, step);
    let workflowJson = localStorage.getItem("workflow");
    workflowJson = JSON.parse(workflowJson);
    const allSteps = [...steps];
    workflowJson = {
      ...workflowJson,
      steps: allSteps,
    };
    localStorage.setItem("workflow", JSON.stringify(workflowJson));
    addStep(allSteps);
  };

  const renameForm = (step) => () => {
    setSelectedStep(step);
    openStepperModal(true);
  };

  const deleteAllForms = () => {
    localStorage.clear();
    addStep([]);
  };

  const addStepper = (stepName) => {
    if (!stepName) {
      openStepperModal(false);
      setSelectedStep(null);
      return;
    }
    const allSteps = [...steps];
    if (selectedStep) {
      selectedStep.stepName = stepName;
    } else {
      allSteps.push({
        stepId: uuidv4(),
        stepName,
      });
    }
    // update local storage with new step
    let workflowJson = localStorage.getItem("workflow");
    workflowJson = JSON.parse(workflowJson);
    workflowJson = {
      ...workflowJson,
      steps: allSteps,
    };
    localStorage.setItem("workflow", JSON.stringify(workflowJson));
    addStep(allSteps);
    openStepperModal(false);
    setSelectedStep(null);
  };

  const readFileData = (evt) => {
    const file = evt.target.files[0];
    const fileReader = new FileReader();
    fileReader.onload = (textEvent) => {
      let text = textEvent.target.result;
      const textJson = JSON.parse(text);
      if (!textJson) {
        alert("Seems invalid file...");
        return;
      }
      localStorage.setItem("workflow", text);
      addStep(textJson.steps);
    };
    fileReader.readAsText(file);
  };

  return (
    <div style={{ display: "flex", margin: "5% 10%" }}>
      <DragArea />
      {(steps.length || "") && (
        <div style={{ marginTop: "-90px", width: "60%" }}>
          <StepZilla
            key={steps.length}
            showNavigation={false}
            steps={_.map(steps, (step) => ({
              name: step.stepName,
              component: (
                <DropArea
                  key={step.stepId}
                  stepId={step.stepId}
                  createNewForm={createNewForm}
                  renameForm={renameForm(step)}
                  deleteForm={deleteForm(step)}
                  deleteAllForms={deleteAllForms}
                />
              ),
            }))}
          />
        </div>
      )}
      {/* // when no steps are added show simple drop area */}
      {!steps.length && (
        <div
          style={{
            width: "60%",
            display: "flex",
            justifyContent: "space-around",
            alignItems: "center",
          }}
        >
          {/* <DropArea createNewForm={createNewForm} showSave={false} /> */}
          <Button
            content="Load Json File"
            labelPosition="left"
            icon="file"
            onClick={() => fileInputRef.current.click()}
          />
          <input
            ref={fileInputRef}
            type="file"
            hidden
            onChange={readFileData}
          />
          <Button primary onClick={createNewForm}>
            Start new
          </Button>
        </div>
      )}
      {stepperModalOpen && (
        <StepperModal onClose={addStepper} selectedStep={selectedStep} />
      )}
    </div>
  );
}

export default FormBuilder;
