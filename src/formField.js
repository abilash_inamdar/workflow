import React from "react";
import { useDrag } from "react-dnd";
import { Icon } from "semantic-ui-react";

function FormField({ icon, label, field }) {
  const [{ isDragging }, drag] = useDrag({
    item: {
      type: "FORMFIELD",
      field
    },
    collect: monitor => ({ isDragging: !!monitor.isDragging })
  });

  console.log("isDragging", isDragging);
  return (
    <div
      ref={drag}
      style={{
        cursor: "move",
        margin: "5px",
        padding: "10px",
        border: "1px dashed #ddd"
      }}
    >
      <p>
        <Icon name={icon} />
        {label}
      </p>
    </div>
  );
}

export default FormField;
