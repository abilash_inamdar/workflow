import React from 'react';
import ReactDOM from 'react-dom';
import 'react-semantic-ui-datepickers/dist/react-semantic-ui-datepickers.css';
import 'react-phone-input-2/lib/style.css'
import "react-stepzilla/src/css/main.css";
import "semantic-ui-css/semantic.min.css";
import './index.css'
import App from './App';
import * as serviceWorker from './serviceWorker';

ReactDOM.render(<App />, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
