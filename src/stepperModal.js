import React, { useState } from "react";
import _ from "lodash";
import { Modal, Button, Input, Header } from "semantic-ui-react";

function StepperModal({ onClose, selectedStep }) {
  const [name, setName] = useState(_.get(selectedStep, "stepName", ""));
  return (
    <Modal open closeIcon onClose={() => onClose()}>
      <Modal.Header>Stepper</Modal.Header>
      <Modal.Content>
        <Modal.Description>
          <Header>Name your Step</Header>
          <Input
            fluid
            placeholder="Step name"
            value={name}
            onChange={(e) => setName(e.target.value)}
          />
          <div style={{ textAlign: "center", marginTop: "10px" }}>
            <Button
              disabled={!name.trim().length}
              primary
              onClick={() => onClose(name)}
            >
              Done
            </Button>
          </div>
        </Modal.Description>
      </Modal.Content>
    </Modal>
  );
}

export default StepperModal;
